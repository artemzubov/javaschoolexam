package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if(x == null || y == null){
            throw new IllegalArgumentException("One (or both) of the lists is(are) null");
        }
        else if (x.equals(y)){
            return true;
        }
        else {
            for (Object objectY : y) {

                if (x.isEmpty()){
                    return true;
                }

                if (objectY.equals(x.get(0))) {
                    x.remove(0);
                }
            }
        }

        return false;
    }
}
