package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int pyramidFooting = getPyramidFooting(inputNumbers.size());

        if (pyramidFooting <= 0){

            throw new CannotBuildPyramidException("Incorrect quantity of items for pyramid");
        }
        else if (pyramidFooting == 1){

            int[][] pyramid = new int[pyramidFooting][pyramidFooting];

            pyramid[0][0] = inputNumbers.get(0);

            return pyramid;
        }
        else {

            List<Integer> modifiableList = sortAndCheckValid(inputNumbers);

            int[][] pyramid = new int[pyramidFooting][pyramidFooting*2-1];

            for (int rows = 0; rows < pyramid.length; rows++){

                int quantityOfItemsToPutInTheRow = rows+1;

                //for (int columns = pyramidFooting-quantityOfItemsToPutInTheRow; quantityOfItemsToPutInTheRow > 0; columns += 2, quantityOfItemsToPutInTheRow--){
                for (int columns = pyramidFooting-quantityOfItemsToPutInTheRow; quantityOfItemsToPutInTheRow > 0;   ){

                    pyramid[rows][columns] = modifiableList.remove(0);
                    columns += 2;
                    quantityOfItemsToPutInTheRow--;
                }
            }

            return pyramid;
        }
    }

    //returns 0, if pyramid cannot by built with given quantity of elements
    private int getPyramidFooting(int givenQuantityOfItems){

        int quantityOfPyramidItems = 1;
        int pyramidFooting = 1;

        while (quantityOfPyramidItems <= givenQuantityOfItems){

            if (quantityOfPyramidItems == givenQuantityOfItems){

                return pyramidFooting;
            }

            pyramidFooting += 1;
            quantityOfPyramidItems += pyramidFooting;
        }

        return 0;
    }

    private List<Integer> sortAndCheckValid(List<Integer> list){
        return list.stream()
            .sorted((o1, o2) -> {

                if (o1 == null || o2 == null){

                    throw new CannotBuildPyramidException("There is null in the List");
                }
                else if (o1.equals(o2)){

                    throw new CannotBuildPyramidException("There is equal elements in the List");
                }
                return o1.compareTo(o2);

            }).collect(Collectors.toList());
    }
}
