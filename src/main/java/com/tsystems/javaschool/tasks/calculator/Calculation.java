package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculation {

    public static final Map<String, Integer> MATH_OPERATIONS;

    static {
        MATH_OPERATIONS = new HashMap<>();
        MATH_OPERATIONS.put("*", 1);
        MATH_OPERATIONS.put("/", 1);
        MATH_OPERATIONS.put("+", 2);
        MATH_OPERATIONS.put("-", 2);
    }

    public static String sortingStation(String expression, Map<String, Integer> operations) {

        if (expression == null || expression.length() == 0)
            throw new IllegalStateException("Expression isn't defined");
        if (operations == null || operations.isEmpty())
            throw new IllegalStateException("Operations aren't defined");
        if (expression.contains("//")
                || expression.contains("**")
                || expression.contains("--")
                || expression.contains("++")){
            throw new IllegalStateException("Expression contains error - double math expressions(+-/*)");
        }

        List<String> out = new ArrayList<>();

        Stack<String> stack = new Stack<>();

        expression = expression.replace(" ", "");

        Set<String> operationSymbols = new HashSet<>(operations.keySet());
        String leftBracket = "(";
        String rightBracket = ")";
        operationSymbols.add(leftBracket);
        operationSymbols.add(rightBracket);

        int index = 0;

        boolean findNext = true;
        while (findNext) {
            int nextOperationIndex = expression.length();
            String nextOperation = "";

            for (String operation : operationSymbols) {
                int i = expression.indexOf(operation, index);
                if (i >= 0 && i < nextOperationIndex) {
                    nextOperation = operation;
                    nextOperationIndex = i;
                }
            }

            if (nextOperationIndex == expression.length()) {
                findNext = false;
            } else {

                if (index != nextOperationIndex) {
                    out.add(expression.substring(index, nextOperationIndex));
                }

                if (nextOperation.equals(leftBracket)) {
                    stack.push(nextOperation);
                } else if (nextOperation.equals(rightBracket)) {
                    while (!stack.peek().equals(leftBracket)) {
                        out.add(stack.pop());
                        if (stack.empty()) {
                            throw new IllegalArgumentException("Unmatched brackets");
                        }
                    }
                    stack.pop();
                } else {
                    while (!stack.empty() && !stack.peek().equals(leftBracket) &&
                            (operations.get(nextOperation) >= operations.get(stack.peek()))) {
                        out.add(stack.pop());
                    }
                    stack.push(nextOperation);
                }
                index = nextOperationIndex + nextOperation.length();
            }
        }

        if (index != expression.length()) {
            out.add(expression.substring(index));
        }

        while (!stack.empty()) {
            out.add(stack.pop());
        }
        StringBuilder result = new StringBuilder();
        if (!out.isEmpty())
            result.append(out.remove(0));
        while (!out.isEmpty())
            result.append(" ").append(out.remove(0));

        return result.toString();
    }

    public static Double calculateExpression(String expression) {

        String rpn = sortingStation(expression, MATH_OPERATIONS);
        StringTokenizer tokenizer = new StringTokenizer(rpn, " ");
        Stack<Double> stack = new Stack<>();
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();

            if (!MATH_OPERATIONS.keySet().contains(token)) {
                stack.push(Double.parseDouble(token));
            } else {
                Double secondNumber = stack.pop();
                Double firstNumber = stack.pop();
                switch (token) {
                    case "*":
                        stack.push(firstNumber * secondNumber);
                        break;
                    case "/":
                        if (secondNumber == 0) {
                            throw new ArithmeticException("divide by zero");
                        }
                        stack.push(firstNumber / secondNumber);
                        break;
                    case "+":
                        stack.push(firstNumber + secondNumber);
                        break;
                    case "-":
                        stack.push(firstNumber - secondNumber);
                        break;
                }
            }
        }
        if (stack.size() != 1){
            throw new IllegalArgumentException("Expression syntax error");
        }

        return stack.pop();
    }
}